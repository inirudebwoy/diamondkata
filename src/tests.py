import unittest

from diamond import make_diamond, make_row, \
    get_number_of_front_spaces, get_number_of_middle_spaces, \
    make_first_half_sequence, make_second_half_sequence, \
    make_sequence


class Diamond(unittest.TestCase):
    def test_a_diamond(self):
        self.assertEquals(make_diamond('a'), 'a')

    def test_a_diamond_a_row(self):
        self.assertEquals(make_row('a', 'a'), 'a')

    def test_b_diamond_a_row(self):
        self.assertEquals(make_row('b', 'a'), ' a')

    def test_h_diamond_a_row(self):
        self.assertEquals(make_row('h', 'a'), '       a')

    def test_number_of_front_spaces_in_combo_d_b(self):
        self.assertEquals(get_number_of_front_spaces('d', 'b'), 2)

    def test_get_number_of_middle_spaces_in_combo_d_b(self):
        self.assertEquals(get_number_of_middle_spaces('d', 'b'), 1)

    def test_b_diamond_b_row(self):
        self.assertEquals(make_row('b', 'b'), 'b b')

    def test_d_diamond_b_row(self):
        self.assertEquals(make_row('d', 'b'), '  b b')

    def test_d_diamond_a_row(self):
        self.assertEquals(make_row('d', 'a'), '   a')

    def test_make_first_half_sequence(self):
        self.assertEquals(make_first_half_sequence('c'), 'ab')

    def test_make_second_half_sequence(self):
        self.assertEquals(make_second_half_sequence('c'), 'ba')

    def test_make_sequence_c(self):
        self.assertEquals(make_sequence('c'), 'abcba')

if __name__ == '__main__':
    unittest.main()
