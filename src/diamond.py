ref_list = "abcdefghij"

def make_diamond(letter):
    return make_row(letter, ref_list[0])

def make_row(target_letter, row_letter):
    if row_letter == 'a':
        return ' ' * get_number_of_front_spaces(target_letter,
                                            row_letter) + row_letter
    return ' ' * get_number_of_front_spaces(target_letter,
                                            row_letter) + row_letter + \
        ' ' * get_number_of_middle_spaces(target_letter,
                                          row_letter) + row_letter

def get_number_of_front_spaces(target_letter, row_letter):
    i = ref_list.index(target_letter)
    j = ref_list.index(row_letter)
    return i - j

def get_number_of_middle_spaces(target_letter, row_letter):
    i = ref_list.index(target_letter)
    j = ref_list.index(row_letter)
    return 2 * j - 1

def make_first_half_sequence(letter):
    return ref_list[:ref_list.index(letter)]

def make_second_half_sequence(letter):
    return make_first_half_sequence(letter)[::-1]

def make_sequence(letter):
    return make_first_half_sequence(letter) + letter +\
        make_second_half_sequence(letter)
